import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './base.signup.component.html',
  styleUrls: ['./base.signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  bgImg;
  isSubmitted = false;
  constructor(private _auth: AuthService, private fb: FormBuilder, private _ds:DataService, private http: HttpClient) { }

  ngOnInit(): void {
    this.signupForm =  this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      company: ['', Validators.required],
      designation: ['', Validators.required],
      mobile: ['***', Validators.required],
      city: ['***', Validators.required],
      gender: ['male', Validators.required],
      is_founded: ['**', Validators.required],
      policy: ['**', Validators.required],
    });
//     this._ds.getSettingSection().subscribe(res=>
//     document.getElementById("bg").style.backgroundImage = "url("+'background.jpg'+")"
//     // console.log(res['bgImages']['signup'])
// );
  }
  submitSignup(data) {
    console.log('erespone', this.signupForm.value)
    this.isSubmitted = true;
    const formData = new FormData();
    formData.append('name', this.signupForm.value.name);
    formData.append('email', this.signupForm.value.email);
    formData.append('company', this.signupForm.value.company);
    formData.append('designation', this.signupForm.value.designation);
    formData.append('mobile', this.signupForm.value.mobile);
    formData.append('city', '***');
    formData.append('gender', '***');
    formData.append('is_founded', '***');
    formData.append('tnc', '***');
    if (this.signupForm.valid) {
      this._auth.signupMethod(formData).subscribe((res: any) => {
        if (res.code == 1) {
          this.isSubmitted = false;
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Registration Success!!',
            showConfirmButton: false,
            timer: 3000
          });
          data.resetForm(); 
          this.resetForm();
        } else {          
          data.resetForm();          
          this.resetForm();
        }
        
      });
    } else {
      console.log("thisis msg")
    }
  }
  resetForm(){
    this.signupForm.patchValue({
      mobile: '***',
      city: '***',
      gender: 'male',
      is_founded: '**',
      policy: '**'
    });
  }
}
