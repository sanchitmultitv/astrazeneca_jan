// import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Lobby1RoutingModule } from './lobby1-routing.module';
import { Lobby1Component } from './lobby1.component';
import { HttpClientModule } from '@angular/common/http';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


@NgModule({
  declarations: [Lobby1Component],
  imports: [
    CommonModule,
    Lobby1RoutingModule,
    HttpClientModule,
    PdfmodalModule,RouterModule
  ]
})
export class Lobby1Module { }
