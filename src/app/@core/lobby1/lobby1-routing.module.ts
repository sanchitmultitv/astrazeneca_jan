import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Lobby1Component } from './lobby1.component';

const routes: Routes = [{ path: '', component: Lobby1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Lobby1RoutingModule { }
