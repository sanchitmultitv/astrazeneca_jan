import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { DataService, localService } from 'src/app/services/data.service';


 declare var $:any;
declare var introJs;
@Component({
  selector: 'app-lobby1',
  templateUrl: './lobby1.component.html',
  styleUrls: ['./lobby1.component.scss']
})

export class Lobby1Component implements OnInit,OnDestroy {
  isVideoPlayed = false;
 
  gotoPath;
  sName;
  point = [];
  json=[]
  lobbyImage;

  srcc:any;
  constructor(private router: Router, private _ds: DataService, private _auth:AuthService, private _ls:localService) { }

  ngOnInit(): void {
   // this._ls.stepUpAnalytics('click_lobby');
    //this._ls.stepUpAnalytics('click_helpdesk');
 
   /* this._ds.jsonFile().subscribe((res:any)=>{
    this.json=res
    console.log(this.json)
   }) */
   this._auth.settingItems$.subscribe(items => {
    this.point = items.length?items[0]["lobbyPointers1"]:items;
     console.log(this.point);
    //  this.lobbyImage = items.length?items[0]["bgImages"].lobby:items;
    // console.log(this.lobbyImage); 
   
    // if(localStorage.getItem('tour_guide')==='start_guide')
    // this.startTour();
  });
  
  }

  // ngAfterViewInit(){
  //   this._auth.getMessages().subscribe(data=>{
  //     this.pointers = data["lobbyPointers"]; 
  //     this.startTour();
  //   });
   
  // }
  
  pointerMethod(item){
   // alert(item.path)
    this._ls.stepUpAnalytics('click_'+item.path);
    if(item.path=='exhibition/home/1'){
      this.router.navigateByUrl('exhibition/home/1')
    }else if(item.path=='auditorium/full/205109'){
      this.router.navigateByUrl('auditorium/full/205109')
    }else{
      $('#lobby1Modal').modal('show');
      if(item.title=='top-left'){
       this.srcc='assets/event/CVRM/top-left.jpg'
      }else if(item.title=='top-right'){
       this.srcc='assets/event/CVRM/top-right.jpg'
      }else if(item.title=='left'){
        this.srcc='assets/event/CVRM/left.jpg'
      }else if(item.title=='right'){
        this.srcc='assets/event/CVRM/right.jpg'
      }
    }
 
  }
  closeModal(){
    $('#lobby1Modal').modal('hide');
  }
  // skip(){
  //   const pointerVideo:any = document.getElementById('pointerVideo');
  //   pointerVideo.currentTime = 0;
  //   pointerVideo.pause();
  //   this.router.navigate(['/'+this.gotoPath]);    
  // }
  // endVideo(){
  //   this.router.navigate(['/'+this.gotoPath]);
  // }

  
  ngOnDestroy() {    
    localStorage.removeItem('tour_guide');
  }
}