import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { ExhibitionHallComponent } from './exhibition-hall.component';


@NgModule({
  declarations: [ExhibitionHallComponent],
  imports: [
    CommonModule,
    ExhibitionHallRoutingModule,
   
  ]
})
export class ExhibitionHallModule { }
