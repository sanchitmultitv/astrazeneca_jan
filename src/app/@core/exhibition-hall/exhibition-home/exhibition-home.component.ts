import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { DataService, localService } from 'src/app/services/data.service';
import { DomSanitizer } from '@angular/platform-browser';
import { scryptSync } from 'crypto';
import { exhibition } from '../../../../environments/exhibition'
declare var $: any;
declare var pannellum;
@Component({
  selector: 'app-exhibition-home',
  templateUrl: './exhibition-home.component.html',
  styleUrls: ['./exhibition-home.component.scss']
})
export class ExhibitionHomeComponent implements OnInit {
  ExhibitionList = [];
  hall_id;
  bgImg = 'assets/event/exhibition.jpg';
  totalStageHalls;
  page: number = 1;
  stall_list_rec = {};
  stall_id;
  pointers = [];
  controls: any;

  bg
  ids: string;
  MyData:string=''
  public data:string= this.MyData;

  constructor(private _ls:localService,private _ds: DataService, private router: Router, private _ar: ActivatedRoute, private sanitiser: DomSanitizer) {
    
   }

  images = exhibition
  selectedIndex = -1;
  isSelectedMp4 = false;
  selectedImage;
  ngOnInit(): void {
   // console.log(this.MyData)
    this.panellum();
  
  }

  panellum() {
    pannellum.viewer('panorama', {
      "default": {
        "firstScene": "circle",
      },
      "scenes": {
        "circle": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition.jpg",
          "autoRotate": -2,
          "autoLoad": true,
          "hotSpots": [

            {
              "pitch": 5.5,
              "yaw": 20.5,
              "sceneId": "Primary Care",
              "cssClass": "custom-hoti",
              // createTooltipFunc: this.hotspot,
              //  createTooltipArgs: "click_primary"
            },
            {
              "pitch": 5.5,
              "yaw": 60,
              "sceneId": "Building",
              "cssClass": "custom-hoti",
            },
            {
              "pitch": 5.5,
              "yaw": 83,
              "sceneId": "accelerating",
              "cssClass": "custom-hoti"
            },
            {
              "pitch": 5.5,
              "yaw": 107,
              "sceneId": "oncology",
              "cssClass": "custom-hoti"
             
            },
            {
              "pitch": 5.5,
              "yaw": 153,
              "sceneId": "commercial",
              "cssClass": "custom-hoti"
              
            },
            {
              "pitch": 5.5,
              "yaw": 208,
              "sceneId": "sustainibility",
              "cssClass": "custom-hoti"
             
            },
            {
              "pitch": 5.5,
              "yaw": 253,
              "sceneId": "power",
              "cssClass": "custom-hoti"
          
            },
            {
              "pitch": 5.5,
              "yaw": 277,
              "sceneId": "learning and development",
              "cssClass": "custom-hoti"
            
            },
            {
              "pitch": 5.5,
              "yaw": 300,
              "sceneId": "compliance",
              "cssClass": "custom-hoti"
             
            },
            {
              "pitch": 5.5,
              "yaw": 340,
              "sceneId": "cvrm",
              "cssClass": "custom-hoti"
           
            }
          ]
        },
        "Primary Care": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/PrimaryCare.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "text":"team",
              "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzQyZjA3ZjEtY2MzNC00ZGJjLWJiYWItMzIzMDA3M2M5N2E2%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%220738e544-8f2c-470f-8805-eba27c7d2641%22%7d",
               createTooltipFunc: this.hotspot,
               createTooltipArgs: "primary1"
             
            }
            ,
            {
              "pitch": -18,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -13,
              "id": '773',
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk1"
              
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk1",
              "id": '773'
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "primary"
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 20.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelE-min"
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelF-min"
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 100.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelG-min"
            },
            {
              "pitch": -2.2,
              "yaw": 150.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelH-min"
            },
            {
              "pitch": -2.2,
              "yaw": 212.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelA-min"
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelB-min"
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelC-min"
            },
            {
              "pitch": -2.2,
              "yaw": 340.1,

              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PannelD-min"
            }
          ]

        },
        "Building": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/BuildingHealthyLungs.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_MWVmYjM5NjctZGNhMy00N2Q0LWIxMjQtMDAwODU2MjI2Nzk2%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%22c9809935-f16b-4fc7-b690-432adc04c476%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "building"
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk2",
              "id": 774
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk2",
              "id": 774
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "Video203"
            },
            {
              "pitch": -2.2,
              "yaw": 74.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "Video204"
            },
            {
              "pitch": -2.2,
              "yaw": 262.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "Video201"
            },
            {
              "pitch": -2.2,
              "yaw": 303.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "Video202"
            },
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelC"
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelD"
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelE"
            },
            {
              "pitch": -2.2,
              "yaw": 150.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelF"
            },
            {
              "pitch": -2.2,
              "yaw": 212.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelA"
            },
            {
              "pitch": -2.2,
              "yaw": 337.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelB"
            }
          ]
        },

        "accelerating": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/InnovativeScience-TransformingPatientLives.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzI2NmI1ZjUtNTY0Ny00MDkzLTk5MzMtMzljYWQ3MzFmNDcx%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%22f4a1a79b-85a7-450f-9f11-bf08abb86921%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "innovative"
            
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk3",
              "id": 775
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk3",
              "id": 775
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 150.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "innovative_science"
            },
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "HPanel"
            },
            {
              "pitch": -2.2,
              "yaw": 12.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "IPanel"
            },
            {
              "pitch": -2.2,
              "yaw": 22.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "JPanel"
            },
            {
              "pitch": -2.2,
              "yaw": 72.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "KPanel"
            },
            {
              "pitch": -2.2,
              "yaw": 210.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "APanel"
            },
            {
              "pitch": -2.2,
              "yaw": 257.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "BPanel",
            },
            {
              "pitch": -2.2,
              "yaw": 270.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "CPanel",
            },
            {
              "pitch": -2.2,
              "yaw": 298.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "DPanel",
            },
            {
              "pitch": -2.2,
              "yaw": 309.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "EPanel",
            },
            {
              "pitch": -2.2,
              "yaw": 337.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "FPanel",
            },
            {
              "pitch": -2.2,
              "yaw": 347.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "GPanel",
            }
          ]
        },

        "oncology": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/Oncology.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_MzU0NmM1MmMtNWZmNy00NmFlLWFhYjktN2QxZWNiZjMzZjVl%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%2268e0a2de-5286-4970-9af0-15542f3eb00d%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "oncology"
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk4",
             
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk4",
            
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "reflection"
            },
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelE5",
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelF6",
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelG7",
            },
            {
              "pitch": -2.2,
              "yaw": 150.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelH8",
            },
            {
              "pitch": -2.2,
              "yaw": 212.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelA1",
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelB2",
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelC3",
            },
            {
              "pitch": -2.2,
              "yaw": 340.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelD4",
            }
          ]
        },

        "commercial": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/CommercialEXCELLENCE.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTY1YzBmODYtYmQyOC00NGMyLThmNzMtMjQxNjZiZmM1OGFm%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%2280486379-0d6e-4755-8989-a1b05327e9dc%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "commercialexcellence"
             
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk5",
              
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk5",
             
            },
            // ,
            // {
            //  // "pitch": -2.2,
            //   //"yaw": 0.1,
            //  // createTooltipFunc: this.hotspot,
            //   //"createTooltipArgs": ""
            // },
            {
              "pitch": -2.2,
              "yaw": 74.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "cs4"
            },
            {
              "pitch": -2.2,
              "yaw": 178.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "cs1"
            },
            {
              "pitch": -2.2,
              "yaw": 283.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "cs2"
            },
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelE15",
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelF16",
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelG17",
            },
            {
              "pitch": -2.2,
              "yaw": 150.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelH18",
            },
            {
              "pitch": -2.2,
              "yaw": 212.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelA11",
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelB12",
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelC13",
            },
            {
              "pitch": -2.2,
              "yaw": 340.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelD14",
            }
          ]
        },

        "sustainibility": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/ACCESSiskey,SUSTAINABILITYmatters.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_NzllYmNiNDYtNzI4ZC00NGM2LWEyZWQtNWQ5ZTY2OGE5MWFi%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%221121bb41-52d4-47c7-83bc-a38801145df6%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "sustainiblity"
             
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            { 
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk6",
           
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk6",
            
            }
            ,
            {
              "pitch": -2.2,
              "yaw": -14.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "AZ_Sustainability"
            },
            {
              "pitch": -2.2,
              "yaw": 15.3,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "video3"
            },
            {
              "pitch": -2.2,
              "yaw": 54.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelD24"
            },
            {
              "pitch": -2.2,
              "yaw": 179.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "Wall_ISHIC"
            },
            {
              "pitch": -2.2,
              "yaw": 150.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelF26",
            },
            {
              "pitch": -2.2,
              "yaw": 212.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelA21",
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelB22",
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelC23",
            },
            {
              "pitch": -2.2,
              "yaw": 455.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelE25",
            }
          ]
        },

        "power": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/PowerOfPeopleCorner.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODI4ODBhMjYtN2FlNS00NWM3LWEzNjMtZTRhN2M3NDk3MThk%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%227b2e0397-ed3f-4782-81fc-d32a6abc42b1%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "power"
              
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk7",
            
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk7",
            
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "powerpeople_corner"
            },
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "EPanel35",
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "FPanel36",
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "GPanel37",
            },
            {
              "pitch": -2.2,
              "yaw": 173.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "APanel31",
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "BPanel32",
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "CPanel33",
            },
            {
              "pitch": -2.2,
              "yaw": 340.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "DPanel34",
            }
          ]
        },

        "learning and development": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/CommercialLEARNING&DEVELOPMENT.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWE0OTFhNDMtMzczMC00ZmEyLTk0ODItOGUwZTljZDFhMGZl%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%2206f98f00-f1f1-495d-be9c-e6aec16468e0%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "learning"
            
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk8",
          
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk8",
            
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "coomercial_learning_devbooth"
            },
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "EPanel45",
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "FPanel46",
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "GPanel47",
            },
            {
              "pitch": -2.2,
              "yaw": 173.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "APanel41",
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "BPanel42",
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "CPanel43",
            },
            {
              "pitch": -2.2,
              "yaw": 340.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "DPanel44",
            }
          ]
        },
        "compliance": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/IamCompliance.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZTdiYTI4ZDUtOThhZS00MTc4LWI5MGEtM2E3YTUxZWEyYjU5%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%222eb2cec8-12f3-40b5-857f-0d7ce4f11f3a%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "compliance"
              
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk9",
          
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk9",
       
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "complaince_champions"
            },
            {
              "pitch": -2.2,
              "yaw": 342.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "sustainability_onboarding_video_with_english"
            },
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "EPanel55",
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "FPanel56",
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "GPanel57",
            },
            {
              "pitch": -2.2,
              "yaw": 147.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "HPanel58",
            },
            {
              "pitch": -2.2,
              "yaw": 178.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "APanel51",
            },
            {
              "pitch": -2.2,
              "yaw": 205.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "BPanel52",
            },
            {
              "pitch": -2.2,
              "yaw": 263.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "CPanel53",
            },
            {
              "pitch": -2.2,
              "yaw": 302.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "DPanel54",
            }
          ]
        },
        "cvrm": {
          "type": "equirectangular",
          "panorama": "assets/event/exhibition/CVRM.jpg",

          "hotSpots": [
            {
              "pitch": -12,
              "yaw": 1,
              "URL":"https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmJmMTE5MjEtMzUwNy00YjgzLTliZmMtYzU5YmNkMjhkN2Rm%40thread.v2/0?context=%7b%22Tid%22%3a%22af8e89a3-d9ac-422f-ad06-cc4eb4214314%22%2c%22Oid%22%3a%2237fda907-f5b0-4704-be67-4b42eb62c5ce%22%7d",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "cvrm"
            }
            ,
            {
              "pitch": -16,
              "yaw": -1,
              "sceneId": "circle",
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "back to hall"
            }
            ,
            {
              "pitch": -12,
              "yaw": -10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk0",
           
            }
            ,
            {
              "pitch": -12,
              "yaw": 10,
              createTooltipFunc: this.hotspot,
              createTooltipArgs: "helpdesk0",
            
            }
            ,
            {
              "pitch": -2.2,
              "yaw": 0.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "1CVRMVideo"
            },
            {
              "pitch": -2.2,
              "yaw": 76.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "OriginalsvsGenerics"
            },
            {
              "pitch": -2.2,
              "yaw": 284.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "CVRM2021Flashback"
            },
            {
              "pitch": -2.2,
              "yaw": 20.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelE65",
            },
            {
              "pitch": -2.2,
              "yaw": 57.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelF66",
            },
            {
              "pitch": -2.2,
              "yaw": 100.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelG67",
            },
            {
              "pitch": -2.2,
              "yaw": 173.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelA61",
            },
            {
              "pitch": -2.2,
              "yaw": 260.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelB62",
            },
            {
              "pitch": -2.2,
              "yaw": 305.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelC63",
            },
            {
              "pitch": -2.2,
              "yaw": 340.1,
              createTooltipFunc: this.hotspot,
              "createTooltipArgs": "PanelD64",
            }
          ]
        }
      }

    });

  }
  imageClasse = 'modal-md';
  hotspot = (hotSpotDiv, args) => {
    // alert(args)
    //console.log(id)
    let arr = [];
    arr.push(args)
    var button = document.createElement("button");
    hotSpotDiv.appendChild(button);
   
     if(args=='primary1' || args=='building'||args=='innovative'||args=='oncology'||args=='commercialexcellence' || args=='sustainiblity'||args=='power'||args=='learning'||args=='compliance'||args=='cvrm'){
      this._ls.stepUpAnalytics('click_'+args)
    }
    if (args == 'helpdesk1'|| args=='helpdesk2'||args=='helpdesk3' ||args=='helpdesk4' ||args=='helpdesk5' ||args=='helpdesk6' || args=='helpdesk6'||args=='helpdesk7'||args=='helpdesk8'||args=='helpdesk9'||args=='helpdesk0') {
      button.innerHTML = '<img  src="assets/icons/help-desk.png" />'
      button.style.background = 'transparent';
      button.style.border = 'none';
      // this._ls.stepUpAnalytics("click_"+args)
     
    }
     else {
      if (args == 'back to hall') {
        button.innerHTML = '<img  src="assets/icons/backLobby.png" />'
        // button.style.cssText="background:white;color:black;padding:5px 10px;border";
        button.style.background = 'transparent';
        button.style.border = 'none';
      }
      else {
        
        button.classList.add("pulsating-circle");
        console.log(args, 'kjh')
        button.style.border = 'none';
        button.style.background = 'transparent';
        button.style.boxShadow = 'none';
        button.style.padding = '0';
        button.style.width = '18px';
      }

    }
  
    button.setAttribute("title", args);
    button.onclick = () => {

      this.selectedIndex = this.images.findIndex(res => res.name == arr[0]);
      let source = this.images[this.selectedIndex]['src'];
      //  else{
        
      if (source.includes('.helpdesk1')) {
          this.MyData='773'
           this._ds.myMethod(this.MyData);
        // $('#test_modal').modal('show');
        $('#welcome_chat_modal').modal('show');
      } else if(source.includes('.helpdesk2')){
        this.MyData='774'
        this._ds.myMethod(this.MyData);
     $('#test_modal').modal('show');
      }
      else if(source.includes('.helpdesk3')){
        this.MyData='775'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else if(source.includes('.helpdesk4')){
        this.MyData='776'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else if(source.includes('.helpdesk5')){
        this.MyData='777'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else if(source.includes('.helpdesk6')){
        this.MyData='778'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else if(source.includes('.helpdesk7')){
        this.MyData='779'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else if(source.includes('.helpdesk8')){
        this.MyData='780'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else if(source.includes('.helpdesk9')){
        this.MyData='781'
        this._ds.myMethod(this.MyData);
     $('#test_modal').modal('show');
      }
      else if(source.includes('.helpdesk0')){
        this.MyData='782'
        this._ds.myMethod(this.MyData);
    //  $('#test_modal').modal('show');
    $('#welcome_chat_modal').modal('show');
      }
      else {
        source = '';
        if (source.includes('.mp4')) {
          this.isSelectedMp4 = true;
          // alert(source)
          this.selectedImage = source;
          this.imageClasse = 'modal-md';
        } else {
          
          // alert(source)
          this.imageClasse = 'modal-lg';

          this.isSelectedMp4 = false;
          this.selectedImage = this.sanitiser.bypassSecurityTrustResourceUrl(this.images[this.selectedIndex]['src']+'#toolbar=0');
          // this.ids=this.images[this.selectedIndex]['id']
          // alert(this.ids)
        }
        $('#popupbreakout_modal').modal('show');
      }
      // }
    }
  }
  
  closeModal() {
    this.isSelectedMp4 = true;
//  let pauseVideo: any = document.getElementById("myVideo2");
//    pauseVideo.currentTime = 0;
//     pauseVideo.pause();
    $('#popupbreakout_modal').modal('hide');
    $('#test_modal').modal('hide');  
    
  }


}