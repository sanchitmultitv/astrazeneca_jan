import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionHomeRoutingModule } from './exhibition-home-routing.module';
import { ExhibitionHomeComponent } from './exhibition-home.component';


@NgModule({
  declarations: [ExhibitionHomeComponent],
  imports: [
    CommonModule,
    ExhibitionHomeRoutingModule,
   
  ]
})
export class ExhibitionHomeModule { }
