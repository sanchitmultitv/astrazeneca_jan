import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NetworkingLoungeRoutingModule } from './networking-lounge-routing.module';
import { NetworkingLoungeComponent } from './networking-lounge.component';
import { ExhibitorChatModule } from '../exhibition-hall/exhibitor-chat/exhibitor-chat.module';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { GroupChatModule } from 'src/app/@main/shared/group-chat/group-chat.module';


@NgModule({
  declarations: [NetworkingLoungeComponent],
  imports: [
    CommonModule,
    NetworkingLoungeRoutingModule,
    ExhibitorChatModule,
    PdfmodalModule,GroupChatModule
    
  ]
})
export class NetworkingLoungeModule { }
