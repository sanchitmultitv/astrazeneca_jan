import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditoriumHomeRoutingModule } from './auditorium-home-routing.module';
import { RouterModule } from '@angular/router';
import { AuditoriumHomeComponent } from './auditorium-home.component';


@NgModule({
  declarations: [AuditoriumHomeComponent],
  imports: [
    CommonModule,
    RouterModule,
    AuditoriumHomeRoutingModule
  ]
})
export class AuditoriumHomeModule { }
