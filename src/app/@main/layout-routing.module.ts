import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  { path:'', redirectTo:'lobby', pathMatch:'full' },
  { path: 'lobby', loadChildren: ()=>import('../@core/lobby/lobby.module').then(m=>m.LobbyModule)},
  { path: 'auditorium', loadChildren: ()=>import('../@core/auditorium/auditorium.module').then(m=>m.AuditoriumModule) },
  { path: 'exhibition', loadChildren: ()=>import('../@core/exhibition-hall/exhibition-hall.module').then(m=>m.ExhibitionHallModule) },
  { path: 'lounge', loadChildren: ()=>import('../@core/networking-lounge/networking-lounge.module').then(m=>m.NetworkingLoungeModule) },
  { path: 'gamezone', loadChildren: ()=>import('../@core/game-zone/game-zone.module').then(m=>m.GameZoneModule) },
  { path: 'photobooth', loadChildren: ()=>import('../@core/photobooth/photobooth.module').then(m=>m.PhotoboothModule) },
  { path: 'helpdesk', loadChildren: ()=>import('../@core/helpdesk/helpdesk.module').then(m=>m.HelpdeskModule) },
  { path: 'signaturewall', loadChildren: ()=>import('../@core/signaturewall/signaturewall.module').then(m=>m.SignaturewallModule) },

  { path: 'lobby1', loadChildren: () => import('../@core/lobby1/lobby1.module').then(m => m.Lobby1Module) },
  { path: 'lobby2', loadChildren: () => import('../@core/lobby2/lobby2.module').then(m => m.Lobby2Module) },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
