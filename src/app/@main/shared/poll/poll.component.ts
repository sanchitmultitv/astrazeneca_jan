import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {
  width;
  progress=20
  pollingList:any =[];
  poll_id;
  showmsg=true;
  showPoll = false;
  pollForm = new FormGroup({
    polling: new FormControl(''),
  });
  msg;
  audi_id;
  shown;
  lol;
  result:[]=[]
 
  constructor(private _ds : DataService,private chat : ChatService) { }
  
  ngOnInit(): void {
   
    this.chat.getSocketMessages().subscribe(((data:any)=>{
      console.log(data)
      let poll = data;
      let polls = poll.split('_');
      if (polls[0] == 'start' && polls[1]=='poll'){

         this.lol=true;
         this.shown=false;

       

          $('#poll_modal').modal('show')
     
        this.poll_id = polls[2];
        console.log(this.poll_id)
        this.audi_id = polls[3];
         this.getPollsList()  
      //  this.getpollresult();   
      }
    }));
  }
 
  closeModal() {
    $('#poll_modal').modal('hide');
  }
  getPollsList() {
    console.log(this.poll_id)
    this._ds.getPollsList(this.poll_id).subscribe((res:any)=>{
      this.pollingList = res.result;
    })
  }
  getpollresult(){
  console.log(this.poll_id)
    this._ds.getPollResult(this.poll_id).subscribe((res:any)=>{
      this.result=res.result.data
      
       // this.progress=res.result.data[1].percentage
     
    //  console.log(this.progress)
    
      console.log(this.result)
    })
  }
  pollSubmit(id){
    let data = JSON.parse(localStorage.getItem('virtual'));    
    this._ds.pollSubmit(id,data.id,this.pollForm.value.polling).subscribe(res=>{
      if(res.code == 1){
        this.getpollresult();
        this.shown=true;
        // Swal.fire({
        //   position: 'top',
        //   icon: 'success',
        //   title: 'Thanks for submitting Poll',
        //   showConfirmButton: false,
        //   timer: 3000
        // });     
      /*   setInterval(function() {
        $('#poll_modal').modal('hide'); 
        }, 4000);  */
          
      }
      this.pollingList = [];
    });
  }

}

