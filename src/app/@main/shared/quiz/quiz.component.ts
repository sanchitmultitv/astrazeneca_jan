import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  questions=[];
  showQuestion = true;
  indx = 0;

  correct;
  total;
  constructor(private _ds:DataService) { }

  ngOnInit(): void {
    this.getResult()
    this._ds.getQuiz().subscribe((res:any)=>{
      this.questions = res.result;
    })
  }
  getResult(){
    let data = JSON.parse(localStorage.getItem('virtual')).id;
    // alert(data)
    this._ds.getQuizResult(data).subscribe((res:any)=>{
      this.correct=res.result.correct
      //alert(this.correct)
      this.total=res.result.summary.length
     // alert(this.total)
    }) 
  }
  closeModal(){
    $("#quiz_modal").modal("hide");
  }
  chooseAnswer(id, option){
    const formData = new FormData();
    const user_id = JSON.parse(localStorage.getItem('virtual')).id;
    formData.append('user_id',user_id);
    formData.append('quiz_id',id);
    formData.append('answer',option);
    document.getElementById('quiz_modal').style.zIndex = '0';
    this._ds.submitQuiz(formData).subscribe(res=>{
      document.getElementById('quiz_modal').style.zIndex = '100000';
      if(res.result=='success!!'){
       // this.getResult()
       
        this.indx++;
        this.getResult()
      }
    });
  }
}
